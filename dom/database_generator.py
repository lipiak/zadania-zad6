# -*- coding: utf-8 -*-

import sqlite3

conn = sqlite3.connect('baza.db')

conn.execute('''create table users
(id integer primary key autoincrement,
login text not null,
haslo text not null
);''')

conn.execute("insert into users(login, haslo) values('admin', 'admin1')")
conn.commit()

conn.execute("insert into users(login, haslo) values('Budzigniew', 'budzigniew')")
conn.commit()

conn.execute("insert into users(login, haslo) values('Andrzyj', 'andrzyj')")
conn.commit()

conn.execute("insert into users(login, haslo) values('Helga', 'helga')")
conn.commit()

conn.execute('''create table wpisy
       (id integer primary key     autoincrement,
       tresc           text    NOT NULL,
       autor           text     NOT NULL,
       data            text    not null
       );''')


print ('baza utworzona')
conn.close()

