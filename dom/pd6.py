# -*- coding: utf-8 -*-

import cherrypy
import sqlite3
from time import gmtime, strftime
from jinja2 import Environment, FileSystemLoader
env = Environment(loader=FileSystemLoader('templates'))



class Mikroblog(object):
    poprzednia_tresc = u''

    @cherrypy.expose
    def index(self):
        tmpl = env.get_template('index.html')
        return tmpl.render(wszystkie_wpisy=self.pobierzWszystkieWpisy(), zalogowany=cherrypy.session.get('loggedIn'))
        #return self.pobierzWszystkieWpisy()
        #return str(self.pobierzWszystkieWpisy())

    @cherrypy.expose
    def doLogin(self, login, haslo):
        conn = sqlite3.connect('baza.db')
        c = conn.cursor()
        c.execute(str.format("select haslo from users where login='{0}'", login))
        wynik = c.fetchone()

        if wynik is None or not wynik.__eq__(haslo):
            tmpl = env.get_template('komunikat.html')
            return tmpl.render(typ='blad', tresc=u'Błędna nazwa użytkonika lub hasło',
                               zalogowany=cherrypy.session.get('loggedIn'))

        else:
            cherrypy.session['loggedIn'] = 'True'
            cherrypy.session['user'] = login
            tmpl = env.get_template('komunikat.html')
            return tmpl.render(typ='ok', tresc=u'Zalogowano pomyślnie.', zalogowany=cherrypy.session.get('loggedIn'))



    @cherrypy.expose
    def logout(self):
        if cherrypy.session['loggedIn'] is 'True':
            cherrypy.session['loggedIn'] = 'False'
            cherrypy.session['user'] = ''
            tmpl = env.get_template('komunikat.html')
            return tmpl.render(typ='ok', tresc=u'Wylogowano poprawnie.', zalogowany=cherrypy.session.get('loggedIn'))
        else:
            tmpl = env.get_template('komunikat.html')
            return tmpl.render(typ='blad', tresc=u'Nie jesteś zalogowany',
                               zalogowany=cherrypy.session.get('loggedIn'))


    @cherrypy.expose
    def login(self):
        if cherrypy.session.get('loggedIn') is 'True':
            tmpl = env.get_template('komunikat.html')
            return tmpl.render(typ='ok', tresc=u'Jesteś już zalogowany.', zalogowany=cherrypy.session.get('loggedIn'))

        else:
            tmpl = env.get_template('login.html')
            return tmpl.render()

    def pobierzWszystkieWpisy(self):
        conn = sqlite3.connect('baza.db')
        c = conn.cursor()
        c.execute('select tresc, autor, data, id from wpisy order by id desc')
        rows = c.fetchall()
        conn.close()
        return rows

    @cherrypy.expose
    def dodawanie(self):
        if cherrypy.session.get('loggedIn') is 'True':
            tmpl = env.get_template('dodawanie.html')
            return tmpl.render(zalogowany=cherrypy.session.get('loggedIn'))
        else:
            tmpl = env.get_template('komunikat.html')
            return tmpl.render(typ='blad', tresc=u'Nie jesteś zalogowany',
                               zalogowany=cherrypy.session.get('loggedIn'))

    @cherrypy.expose
    def dodajWpis(self, tresc):
        if self.poprzednia_tresc.__eq__(tresc):
            tmpl = env.get_template('komunikat.html')
            return tmpl.render(typ='blad', tresc=u'Wystąpił błąd przy dodawaniu komunikatu',
                               zalogowany=cherrypy.session.get('loggedIn'))

        if cherrypy.session.get('loggedIn') is 'True' and not tresc.__eq__(''):
            conn = sqlite3.connect('baza.db')
            query_string = u"insert into wpisy(tresc, autor, data) values('{0}','{1}','{2}')".format(tresc,
                                      cherrypy.session.get('user'), strftime("%Y-%m-%d %H:%M:%S", gmtime()))
            conn.execute(query_string)
            conn.commit()
            conn.close()
            self.poprzednia_tresc = tresc
            tmpl = env.get_template('komunikat.html')
            return tmpl.render(typ='ok', tresc=u'Wpis dodano pomyślnie.', zalogowany=cherrypy.session.get('loggedIn'))
        else:
            tmpl = env.get_template('komunikat.html')
            return tmpl.render(typ='blad', tresc=u'Wystąpił błąd przy dodawaniu komunikatu',
                               zalogowany=cherrypy.session.get('loggedIn'))


def application(environ, start_response):
    cherrypy.tree.mount(Mikroblog(), '/~p7/wsgi', 'config.cfg')
    return cherrypy.tree(environ, start_response)


