# -*- coding: utf-8 -*-

from flask import Flask
from flask import render_template
import bookdb

app = Flask(__name__)
app.config['DEBUG'] = True

db = bookdb.BookDB()

lista_ksiazek = db.titles()

dostepne_id = []

#wypelnij tablice dostepnymi id ksiazek
for ksiazka in lista_ksiazek:
    if ksiazka['id'] not in dostepne_id:
        dostepne_id.append(ksiazka['id'])


@app.route('/')
def books():
    # Przekaż listę książek do szablonu "book_list.html"
    return render_template("book_list.html", lista=lista_ksiazek)


@app.route('/book/<book_id>/')
def book(book_id):
    # Przekaż szczegóły danej książki do szablonu "book_detail.html"
    fullinfo = db.title_info(book_id)
    return render_template("book_detail.html", detale=fullinfo)


if __name__ == '__main__':
    app.run(debug=True)