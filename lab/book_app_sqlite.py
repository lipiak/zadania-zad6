# -*- coding: utf-8 -*-

from flask import Flask, render_template
import sqlite3
from contextlib import closing
from flask import g, session
from flask import request, redirect, abort
from flask import flash, url_for

#################
#  Konfiguracja #
#################

DATABASE = 'books.db'
SECRET_KEY = '1234567890!@#$%^&*()'

USERNAME = 'admin'
PASSWORD = 'TajneHaslo'


#############
# Aplikacja #
#############

app = Flask(__name__)
app.config.from_object(__name__)



@app.route('/')
def show_entries():
    lista = get_all_entries()
    return render_template('book_list.html', lista=lista)


###############
# Baza danych #
###############

def connect_db():
    return sqlite3.connect(app.config['DATABASE'], check_same_thread=False)


def init_db():
    with closing(connect_db()) as db:
        with app.open_resource('layout.sql') as f:
            db.cursor().executescript(f.read())
        db.commit()

    g.db.execute('insert into books(title, isbn, author, publisher) values (?, ?, ?, ?)',
                 ['tytul', 'isbn', 'autor', 'pub'])
    g.db.commit()


@app.before_request
def before_request():
    g.db = connect_db()


@app.teardown_request
def teardown_request(exception):
    g.db.close()


def write_entry(title, text):
    pass


def get_all_entries():
    cur = g.db.execute('select id, title from books order by id desc')
    # Stworzenie słownika dla każdego odczytanego wiersza oraz upakowanie ich w listę
    entries = [dict(id=row[0], title=row[1]) for row in cur.fetchall()]
    return entries



################
# Autentykacja #
################

def do_login(usr, pwd):
    if usr != app.config['USERNAME']:
        raise ValueError
    elif pwd != app.config['PASSWORD']:
        raise ValueError
    else:
        session['logged_in'] = True



################
# Uruchomienie #
################

if __name__ == '__main__':
    app.run(debug=True)
    init_db()